package com.deviget.android.listeners;

/**
 * Created by achercasky on 23/07/2019.
 */
public interface OnLoadMoreListener {
    /**
     * Should load more items
     */
    void onLoadMore();
}