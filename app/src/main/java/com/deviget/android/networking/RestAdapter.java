package com.deviget.android.networking;

import com.deviget.android.BuildConfig;
import com.deviget.android.api.RedditApi;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by achercasky on 22/07/2019.
 */
public class RestAdapter {

    private static final int TIMEOUT = 30;

    private static final RestAdapter INSTANCE = new RestAdapter();

    private RedditApi mApi;

    private Retrofit mRetrofit;

    private RestAdapter() {
    }

    /**
     * Factory method
     *
     * @return a singleton of SorRestAdapter
     */
    public static RestAdapter getInstance() {
        return INSTANCE;
    }

    public RedditApi getService() {
        if (mRetrofit == null || mApi == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .client(getOkHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(MoshiConverterFactory.create(getMoshi()))
                    .build();

            mApi = mRetrofit.create(RedditApi.class);
        }

        return mApi;
    }

    private OkHttpClient getOkHttpClient() {
        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        return new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    private Moshi getMoshi() {
        return new Moshi.Builder()
                .add(new KotlinJsonAdapterFactory())
                .build();
    }
}
