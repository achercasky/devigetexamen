package com.deviget.android.types;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.deviget.android.types.StatusType.ERROR;
import static com.deviget.android.types.StatusType.SUCCESS;

/**
 * Created by achercasky on 22/07/2019.
 */
@Retention(RetentionPolicy.SOURCE)
@StringDef({SUCCESS, ERROR})
public @interface StatusType {
    String SUCCESS = "success";
    String ERROR = "error";
}
