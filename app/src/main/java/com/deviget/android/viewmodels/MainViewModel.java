package com.deviget.android.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.chachapps.initial.mvvm.viewmodels.BaseViewModel;
import com.deviget.android.api.RedditModel;
import com.deviget.android.dtos.ErrorResponse;
import com.deviget.android.dtos.RedditResponse;
import com.deviget.android.dtos.Resource;
import com.deviget.android.subscribers.BaseSubscriber;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by achercasky on 22/07/2019.
 */
public class MainViewModel extends BaseViewModel {

    private MutableLiveData<Resource> mResult;

    @Override
    public LiveData getData() {
        if (mResult == null) {
            mResult = new MutableLiveData<>();
        }

        return mResult;
    }

    public void getTopPosts() {
        final Disposable disposable = RedditModel.getInstance().getTopPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new BaseSubscriber<RedditResponse>() {
                    @Override
                    public void onNext(final RedditResponse redditResponse) {
                        mResult.postValue(Resource.Companion.success(redditResponse.getData().getChildren()));
                    }

                    @Override
                    public void onError(@NonNull final ErrorResponse error) {
                        mResult.postValue(Resource.Companion.error("Algo salio mal", error));
                    }
                });

        compositeDisposable.add(disposable);
    }
}
