package com.deviget.android.viewmodels.observers;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.deviget.android.dtos.Resource;
import com.deviget.android.types.StatusType;

/**
 * Created by achercasky on 22/07/2019.
 */
public abstract class CustomObserver<T> implements Observer<Resource> {

    @Override
    public void onChanged(@Nullable Resource resource) {

        if (StatusType.ERROR.equalsIgnoreCase(resource.getStatus())) {
            onError();
        } else {
            onSuccess((T) resource.getData());
        }
    }

    public abstract void onSuccess(T t);

    public abstract void onError();
}
