package com.deviget.android.subscribers;

import android.support.annotation.NonNull;

import com.deviget.android.dtos.ErrorResponse;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

/**
 * Created by achercasky on 22/07/2019.
 */
public abstract class BaseSubscriber<T> extends DisposableObserver<T> {

    @Override
    public void onError(@NonNull final Throwable t) {

        if (t instanceof HttpException) {

            if (((HttpException) t).code() == 401) {
                onError(ErrorResponse.unAuthorized(((HttpException) t).code()));
            } else {
                onError(ErrorResponse.httpError(t));
            }
        } else {
            onError(ErrorResponse.unexpectedError(t));
        }
    }

    /**
     * Called on error event
     *
     * @param error the error response
     */
    public abstract void onError(@NonNull ErrorResponse error);

    @Override
    public void onComplete() {
        //Nothing to do here...
    }
}