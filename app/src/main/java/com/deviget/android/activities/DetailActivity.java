package com.deviget.android.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.deviget.android.R;
import com.deviget.android.dtos.Post;

/**
 * Created by achercasky on 23/07/2019.
 */
public class DetailActivity extends BaseActivity {

    private static final String EXTRA_PARAM_POST = "EXTRA_PARAM_POST";

    public static Intent getIntent(Context context, Post post) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_PARAM_POST, post);

        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        displayBack();

        showMainLayout();

        Post post = getIntent().getParcelableExtra(EXTRA_PARAM_POST);

        ((TextView) findViewById(R.id.activity_detail_title)).setText(post.getAuthor());
        ((TextView) findViewById(R.id.activity_detail_description)).setText(post.getTitle());

        Glide
                .with(this)
                .load(post.getThumbnail())
                .into((ImageView) findViewById(R.id.activity_detail_image));
    }
}
