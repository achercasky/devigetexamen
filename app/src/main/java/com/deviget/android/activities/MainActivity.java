package com.deviget.android.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.deviget.android.R;
import com.deviget.android.adapters.MainAdapter;
import com.deviget.android.dtos.Children;
import com.deviget.android.dtos.Post;
import com.deviget.android.holders.PostViewHolder;
import com.deviget.android.listeners.OnLoadMoreListener;
import com.deviget.android.utilities.DividerItemDecorator;
import com.deviget.android.viewmodels.MainViewModel;
import com.deviget.android.viewmodels.observers.CustomObserver;

import java.util.List;

public class MainActivity extends BaseActivity implements OnLoadMoreListener, PostViewHolder.Callback {

    private MainAdapter mMainAdapter;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private MainViewModel mMainViewModel;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showProgress();

        mMainAdapter = new MainAdapter(this, this);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        RecyclerView recyclerView = findViewById(R.id.activity_main_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecorator(this));
        recyclerView.setAdapter(mMainAdapter);

        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mMainViewModel.getData().observe(this, new CustomObserver<List<Children>>() {
            @Override
            public void onSuccess(final List<Children> children) {
                mMainAdapter.setChildrens(children);
                mSwipeRefreshLayout.setRefreshing(false);
                showMainLayout();
            }

            @Override
            public void onError() {
                showMainLayout();
                Snackbar snackbar = Snackbar.make(recyclerView, getString(R.string.msg_error), Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction(getString(R.string.retry), view -> {
                            showProgress();
                            mMainViewModel.getTopPosts();
                            snackbar.dismiss();
                        });
                snackbar.show();
            }
        });

        mSwipeRefreshLayout = findViewById(R.id.activity_main_swipe);
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mMainViewModel.getTopPosts();
        });

        mMainViewModel.getTopPosts();
    }

    @Override
    public void onLoadMore() {
        mMainAdapter.loadMoreChildren();
    }

    @Override
    public void onDismissPost(int pos) {
        mMainAdapter.removeItem(pos);
    }

    @Override
    public void onImageClicked(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void onPostDetail(Post post) {
        startActivity(DetailActivity.getIntent(this, post));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_action_dismiss_all:
                mMainAdapter.removeAll();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
