package com.deviget.android.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.chachapps.initial.mvvm.activities.AbstractActivity;
import com.chachapps.initial.mvvm.viewmodels.BaseViewModel;
import com.deviget.android.R;

/**
 * Created by achercasky on 22/07/2019.
 */
public abstract class BaseActivity extends AbstractActivity {

    private Toolbar mToolbar;

    @Override
    protected Class<? extends BaseViewModel> initViewModel() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        showToolbar();
    }

    @Override
    protected int getProgressLayoutResourceId() {
        return R.layout.view_progress;
    }

    @Override
    protected int getEmptyLayoutResourceId() {
        return R.layout.view_empty;
    }

    @Override
    protected int getErrorLayoutResourceId() {
        return R.layout.view_error;
    }

    @Override
    protected int getToolbarLayoutResourceId() {
        return R.layout.view_toolbar;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    protected void displayBack() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}