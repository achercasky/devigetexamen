package com.deviget.android.dtos;

import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.HttpException;

/**
 * Created by achercasky on 22/07/2019.
 */
public class ErrorResponse {
    private static final int ERROR_UNEXPECTED = 0;
    public static final int ERROR_NETWORK = 1;
    public static final int ERROR_HTTP = 2;
    public final int type;
    public final String message;
    public final int status;

    private static final String EMPTY = "";

    /* default */ ErrorResponse(final Builder builder) {
        this.type = builder.errorType;
        this.message = builder.errorMessage;
        this.status = builder.errorStatus;
    }

    /**
     * Builds an error response of type {@link retrofit2.HttpException}
     *
     * @param t the exception thrown
     * @return the error response
     */
    public static ErrorResponse httpError(@Nullable final Throwable t) {

        final int errorCode = ((HttpException) t).code();

        String errorMessage = null;

        try {
            final String response = ((HttpException) t).response().errorBody().string();
            JSONObject jsonObject = new JSONObject(response);

            errorMessage = t == null ? EMPTY : jsonObject.getString("message");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new Builder().withMessage(errorMessage).withErrorType(errorCode).build();
    }

    /**
     * Builds an error response of type {@link retrofit2.HttpException}
     *
     * @param errorCode the error http code
     * @return the error response
     */
    public static ErrorResponse httpError(@Nullable final int errorCode) {
        return new Builder().withErrorType(errorCode).build();
    }

    public static ErrorResponse unAuthorized(@Nullable final int errorCode) {
        return new Builder().withErrorType(errorCode).build();
    }

    /**
     * Creates an error response of type unexpected.
     *
     * @param t the exception thrown
     * @return the error response
     */
    public static ErrorResponse unexpectedError(@Nullable final Throwable t) {
        if (t == null) {
            return new Builder().withErrorType(ERROR_UNEXPECTED).build();
        }

        return new Builder().withMessage(t.getMessage())
                .withErrorType(ERROR_UNEXPECTED).build();
    }

    private static final class Builder {
        /* default */ int errorType;
        /* default */ String errorMessage;
        /* default */ int errorStatus;

        /* default */ Builder withErrorType(final int type) {
            this.errorType = type;
            return this;
        }

        /* default */ Builder withMessage(final String message) {
            this.errorMessage = message;
            return this;
        }

        /* default */ Builder withStatus(final int status) {
            this.errorStatus = status;
            return this;
        }

        /* default */ ErrorResponse build() {
            return new ErrorResponse(this);
        }

        @Override
        public String toString() {
            return "Builder{"
                    + "errorType=" + errorType
                    + ", errorMessage='" + errorMessage + '\''
                    + ", errorStatus=" + errorStatus
                    + '}';
        }
    }

    @Override
    public String toString() {
        return "ErrorResponse{"
                + "type=" + type
                + ", message='" + message + '\''
                + ", status=" + status
                + '}';
    }
}
