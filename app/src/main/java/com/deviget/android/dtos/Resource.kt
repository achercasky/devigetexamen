package com.deviget.android.dtos

import com.deviget.android.types.StatusType
import com.deviget.android.types.StatusType.ERROR
import com.deviget.android.types.StatusType.SUCCESS

/**
 * Created by achercasky on 22/07/2019.
 *
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
data class Resource<out T>(@StatusType val status: String?, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(ERROR, data, msg)
        }
    }
}