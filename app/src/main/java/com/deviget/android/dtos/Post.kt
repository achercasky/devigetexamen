package com.deviget.android.dtos

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json

/**
 * Created by achercasky on 22/07/2019.
 */

class Post(
    var title: String?,
    var thumbnail: String? ,
    var created: Long?,
    var description: String?,
    var url: String?,

    @Json(name = "num_comments")
    var comment: Int?,

    @Json(name = "author_fullname")
    var author: String?
) : Parcelable {

    private constructor(parcel: Parcel) : this(
        title = parcel.readString(),
        thumbnail = parcel.readString(),
        created = parcel.readLong(),
        description = parcel.readString(),
        url = parcel.readString(),
        comment = parcel.readInt(),
        author = parcel.readString()
    )

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(title)
        parcel.writeString(thumbnail)
        created?.let { parcel.writeLong(it) }
        parcel.writeString(description)
        parcel.writeString(url)
        comment?.let { parcel.writeInt(it) }
        parcel.writeString(author)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<Post> = object : Parcelable.Creator<Post> {
            override fun createFromParcel(`in`: Parcel): Post {
                return Post(`in`)
            }

            override fun newArray(size: Int): Array<Post?> {
                return arrayOfNulls(size)
            }
        }
    }
}