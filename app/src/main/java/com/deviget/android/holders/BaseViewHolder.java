package com.deviget.android.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by achercasky on 22/07/2019.
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    /**
     * Default constructor
     *
     * @param itemView the layout
     */
    public BaseViewHolder(final View itemView) {
        super(itemView);
    }

    /**
     * Method for rendering data
     *
     * @param object contain the info
     */
    public abstract void onBind(T object);
}
