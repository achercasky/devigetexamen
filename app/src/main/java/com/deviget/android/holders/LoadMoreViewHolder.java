package com.deviget.android.holders;

import android.view.View;
import android.widget.Button;

import com.deviget.android.R;
import com.deviget.android.listeners.OnLoadMoreListener;

/**
 * Created by achercasky on 23/07/2019.
 */
public class LoadMoreViewHolder extends BaseViewHolder{
    /**
     * Default constructor
     *
     * @param itemView the layout
     */
    public LoadMoreViewHolder(View itemView, OnLoadMoreListener listener) {
        super(itemView);

        Button button = itemView.findViewById(R.id.item_load_more_button);
        button.setOnClickListener(v -> listener.onLoadMore());
    }

    @Override
    public void onBind(Object object) {
        // Nothing to do here.
    }
}
