package com.deviget.android.holders;

import android.support.v4.widget.CircularProgressDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.deviget.android.R;
import com.deviget.android.dtos.Post;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by achercasky on 22/07/2019.
 */
public class PostViewHolder extends BaseViewHolder<Post> {

    private TextView mTitle;
    private TextView mDate;
    private TextView mDescription;
    private TextView mComments;
    private TextView mDismiss;

    private ImageView mImage;

    private int mPos;

    private String mUrl;



    private Post mPost;

    /**
     * Default constructor
     *
     * @param itemView the layout
     */
    public PostViewHolder(View itemView, Callback callback) {
        super(itemView);

        itemView.setOnClickListener(view -> callback.onPostDetail(mPost));

        mTitle = itemView.findViewById(R.id.item_post_title);
        mDate = itemView.findViewById(R.id.item_post_date);
        mDescription = itemView.findViewById(R.id.item_post_description);
        mComments = itemView.findViewById(R.id.item_post_comments);

        mImage = itemView.findViewById(R.id.item_post_image);
        mImage.setOnClickListener(v -> {
            if (mUrl != null) {
                callback.onImageClicked(mUrl);
            }
        });

        mDismiss = itemView.findViewById(R.id.item_post_dismiss);
        mDismiss.setOnClickListener(v -> callback.onDismissPost(mPos));
    }

    @Override
    public void onBind(Post post) {

        this.mPost = post;

        this.mUrl = post.getUrl();

        final CircularProgressDrawable drawable = new CircularProgressDrawable(mImage.getContext());
        drawable.setStrokeWidth(5f);
        drawable.setCenterRadius(30f);
        drawable.start();

        mTitle.setText(post.getAuthor());
        mDescription.setText(post.getDescription());

        mComments.setText(post.getComment() + " " + mComments.getContext().getString(R.string.comments));

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        String formattedDate = simpleDateFormat.format(post.getCreated());

        mDate.setText(formattedDate);

        Glide
                .with(mImage.getContext())
                .load(post.getThumbnail())
                .placeholder(drawable)
                .into(mImage);

    }

    public void setPosition(int pos) {
        this.mPos = pos;
    }

    public interface Callback {
        void onDismissPost(int pos);

        void onImageClicked(String url);

        void onPostDetail(Post post);
    }
}
