package com.deviget.android.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.deviget.android.R;
import com.deviget.android.dtos.Children;
import com.deviget.android.holders.BaseViewHolder;
import com.deviget.android.holders.LoadMoreViewHolder;
import com.deviget.android.holders.PostViewHolder;
import com.deviget.android.listeners.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achercasky on 22/07/2019.
 */
public class MainAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    private static final int MAX_ITEMS = 50;

    private int limit = 10;

    private List<Children> mChildrens;

    private List<Children> allPosts;

    private OnLoadMoreListener mOnLoadMoreListener;

    private PostViewHolder.Callback mCallback;

    public MainAdapter(OnLoadMoreListener listener, PostViewHolder.Callback callback) {
        mChildrens = new ArrayList<>();
        allPosts = new ArrayList<>();
        this.mOnLoadMoreListener = listener;
        this.mCallback = callback;
    }

    public void setChildrens(List<Children> childrens) {
        mChildrens.clear();
        allPosts.clear();

        allPosts.addAll(childrens);

        mChildrens = childrens.subList(0, limit);

        notifyDataSetChanged();
    }

    public void loadMoreChildren() {
        int previousLimit = limit;
        limit = limit + 10;

        notifyItemRemoved(mChildrens.size() - 1);

        final List<Children> nextChildrens = allPosts.subList(previousLimit, limit);
        mChildrens.addAll(nextChildrens);
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        if (i == VIEW_TYPE_ITEM) {
            return new PostViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_post, viewGroup, false), mCallback);
        }

        if (i == VIEW_TYPE_LOADING) {
            return new LoadMoreViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_load_more, viewGroup, false), mOnLoadMoreListener);
        }

        throw new AssertionError("The type must be VIEW_TYPE_ITEM or VIEW_TYPE_LOADING.");
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        if (baseViewHolder.getItemViewType() == VIEW_TYPE_ITEM) {
            baseViewHolder.onBind(mChildrens.get(i).getData());
            ((PostViewHolder) baseViewHolder).setPosition(i);
        }
    }

    @Override
    public int getItemCount() {

        int size = mChildrens.size();

        if (size == MAX_ITEMS) {
            return size;
        }

        return mChildrens.isEmpty() ? size : size + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mChildrens.size()) {
            return VIEW_TYPE_LOADING;
        }

        return VIEW_TYPE_ITEM;
    }

    public void removeItem(int pos) {
        mChildrens.remove(pos);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos, getItemCount());
    }

    public void removeAll() {
        int size = mChildrens.size() + 1;
        mChildrens.clear();
        notifyItemRangeRemoved(0, size);
    }
}
