package com.deviget.android.api;

import com.deviget.android.dtos.RedditResponse;
import com.deviget.android.networking.RestAdapter;

import io.reactivex.Observable;

/**
 * Created by achercasky on 22/07/2019.
 */
public class RedditModel {

    private static final RedditModel INSTANCE = new RedditModel();

    public static RedditModel getInstance() {
        return INSTANCE;
    }

    private RedditApi getService() {
        return RestAdapter.getInstance().getService();
    }

    public Observable<RedditResponse> getTopPosts() {
        return getService().getTopPosts();
    }
}