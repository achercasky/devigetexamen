package com.deviget.android.api;

import com.deviget.android.dtos.RedditResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by achercasky on 22/07/2019.
 */
public interface RedditApi {

    @GET("top.json?limit=50")
    Observable<RedditResponse> getTopPosts();
}
